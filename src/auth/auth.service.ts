import { Injectable, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';
import { LoginUserDto } from './dto/login-user.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.login(username);
    const hashedPasswString = user.password.toString();
    const isPasswordMatch = await bcrypt.compare(password, hashedPasswString);
    if (!isPasswordMatch) {
      throw new UnauthorizedException();
    }
    delete user.password;

    return user;
  }

  async login(user: any) {
    const payload = { email: user.email, roles: user.roles, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload)
    }
  }
}
