import { IsEmail, IsString, MinLength, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginUserDto {
  @ApiProperty({
    example: 'an.email@somewebsite.com',
    description: 'a valid email'
  })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    example: 'a password',
    description: 'a valid password. 6 characters minimum'
  })
  @IsString()
  @MinLength(6)
  @IsNotEmpty()
  password: string;
}
