import { IsString, IsNotEmpty, IsInt, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { WalletType } from '../wallet-type.enum';

export class CreateWalletDto {
  @ApiProperty({
    example: 20000000,
    description: 'Amount in Rupiah'
  })
  @IsInt()
  @Min(0)
  @IsNotEmpty()
  amount: number;

  @ApiProperty({
    example: 'A wallet description',
    description: 'A wallet description'
  })
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    example: 'credit',
    description: 'A wallet type. credit, debit, or transfer'
  })
  @IsNotEmpty()
  @IsString()
  type: WalletType;
}
