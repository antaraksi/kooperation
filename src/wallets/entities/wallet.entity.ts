import {
  Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn,
  DeleteDateColumn, VersionColumn, ManyToOne,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { WalletType } from '../wallet-type.enum';

@Entity()
export class Wallet {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('bigint')
  amount: number;

  @Column('text')
  description: string;

  @ManyToOne(() => User, user => user.wallets)
  user: User;

  @Column('varchar', { length: 191 })
  type: WalletType;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @DeleteDateColumn()
  deletedAt: string;

  @VersionColumn()
  version: string;
}
