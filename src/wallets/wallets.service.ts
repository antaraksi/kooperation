import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  paginate, Pagination, IPaginationOptions,
} from '../pagination';
import { Wallet } from './entities/wallet.entity';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { WalletType } from './wallet-type.enum';

@Injectable()
export class WalletsService {
  constructor(
    @InjectRepository(Wallet)
    private walletsRepository: Repository<Wallet>
  ) {}

  async create(createWalletDto: CreateWalletDto) {
    return this.walletsRepository.save(createWalletDto);
  }

  async findAll(
    userId: number,
    paginationOpt: IPaginationOptions,
    keyword?: string
  ): Promise<Pagination<Wallet>> {
    const qb = this.walletsRepository.createQueryBuilder('wallet');
    if (keyword) {
      qb.where('wallet.description LIKE :keyword', { keyword: `%${keyword}%` });
    }
    qb.andWhere('wallet.userId = :userId', { userId });
    qb.orderBy('wallet.id', 'DESC');
    return paginate<Wallet>(qb, paginationOpt);
  }

  findOne(id: number) {
    return this.walletsRepository.findOne(id, {relations: ['user']});
  }

  update(id: number, updateWalletDto: UpdateWalletDto) {
    return this.walletsRepository.update(id, updateWalletDto);
  }

  async remove(id: number) {
    return this.walletsRepository.delete(id);
  }

  async getTotalAmount(userId: number): Promise<number> {
    let debits: Wallet[] = [];
    let credits: Wallet[] = [];
    let transfers: Wallet[] = [];

    const wallets = await this.walletsRepository.createQueryBuilder('wallet')
      .where('wallet.userId = :userId', { userId })
      .getMany();

    debits = wallets.filter(wallet => {
      return wallet.type === WalletType.Debit
    });
    credits = wallets.filter(wallet => {
      return wallet.type === WalletType.Credit
    });
    transfers = wallets.filter(wallet => {
      return wallet.type === WalletType.Transfer
    });

    const totalDebitAmount: number = this.getTotalWalletByType(debits);
    const totalCreditAmount: number = this.getTotalWalletByType(credits);
    const totalTransferAmount: number = this.getTotalWalletByType(transfers);
    return totalDebitAmount - totalCreditAmount - totalTransferAmount;
  }

  getTotalWalletByType(wallets: Wallet[]): number {
    const totalAmount: number = wallets.reduce((accum, item) => accum + +item.amount, 0);
    return totalAmount;
  }
}
