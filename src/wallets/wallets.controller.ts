import {
  Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Request, Query,
  ParseIntPipe, ForbiddenException
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { Pagination } from '../pagination';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { Role } from '../roles/role.enum';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { WalletsService } from './wallets.service';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { Wallet } from './entities/wallet.entity';

@ApiBearerAuth()
@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('wallet')
@Controller('wallets')
export class WalletsController {
  constructor(private readonly walletsService: WalletsService) {}

  @Post()
  @ApiOperation({summary: 'Create a wallet'})
  @ApiResponse({
    status: 201,
    description: 'Wallet is created',
    type: Wallet
  })
  @Roles(Role.Admin)
  create(@Request() req, @Body() createWalletDto: CreateWalletDto) {
    const newWallet = new Wallet();
    newWallet.amount = createWalletDto.amount;
    newWallet.description = createWalletDto.description;
    newWallet.type = createWalletDto.type;
    newWallet.user = req.user.id;
    return this.walletsService.create(newWallet);
  }

  @Get()
  @ApiOperation({summary: 'Get wallet'})
  @ApiResponse({
    status: 200,
    description: 'Get wallets by user id in jwt.',
  })
  @Roles(Role.Admin)
  async findAll(
    @Query('page', ParseIntPipe) page: number = 1,
    @Query('limit', ParseIntPipe) limit: number = 10,
    @Query('keyword') keyword: string,
    @Request() req,
  ): Promise<Pagination<Wallet>> {
    limit = limit > 100 ? 100 : limit;
    const { user } = req;
    const totalAmountWallet = await this.walletsService.getTotalAmount(+user.id);
    return this.walletsService.findAll(user.id, {
      page,
      limit,
      route: '/wallets',
      additional: {
        totalAmountWallet,
      }
    },
    keyword);
  }

  @Get(':id')
  @ApiOperation({summary: 'Get a wallet'})
  @ApiResponse({
    status: 200,
    description: 'Get a wallet info.',
  })
  findOne(@Param('id') id: string) {
    return this.walletsService.findOne(+id);
  }

  @Put(':id')
  @ApiOperation({summary: 'Update a wallet'})
  @ApiResponse({
    status: 200,
    description: 'Update a wallet info.',
  })
  @Roles(Role.Admin, Role.Staff)
  async update(
    @Param('id') id: string,
    @Body() updateWalletDto: UpdateWalletDto,
    @Request() req,
  ) {
    const wallet = await this.walletsService.findOne(+id);
    const { user } = req;
    if ((user.roles === Role.Staff) && (user.id != wallet.user.id)) {
      throw new ForbiddenException;
    }
    return this.walletsService.update(+id, updateWalletDto);
  }

  @Delete(':id')
  @ApiOperation({summary: 'Delete a wallet'})
  @ApiResponse({
    status: 200,
    description: 'Delete a wallet info.',
  })
  async remove(@Param('id') id: string, @Request() req) {
    const wallet = await this.walletsService.findOne(+id);
    const { user } = req;
    if ((user.roles === Role.Staff) && (user.id != wallet.user.id)) {
      throw new ForbiddenException;
    }
    return this.walletsService.remove(+id);
  }
}
