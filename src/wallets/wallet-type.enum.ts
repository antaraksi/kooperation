export enum WalletType {
  Debit = 'debit',
  Credit = 'credit',
  Transfer = 'transfer'
}
