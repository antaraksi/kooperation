import {
  Entity, PrimaryGeneratedColumn, Column, CreateDateColumn,
  UpdateDateColumn, DeleteDateColumn, VersionColumn, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { Wallet } from '../../wallets/entities/wallet.entity';
import { Role } from '../../roles/role.enum';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * The name of the user
   * @example Yudhi
   */
  @Column('longtext')
  name: string;

  @ApiProperty({
    example: 'an.email@somewebsite.com',
    description: 'a valid email'
  })
  @Column('varchar', {
    length: 191,
    unique: true
  })
  email: string;

  @ApiProperty({
    example: 'a password',
    description: 'a valid password. 6 characters minimum'
  })
  @Exclude()
  @Column(
    'binary', {
      length: 60,
      select: false
    },
  )
  password: string;

  @Column('longtext')
  address: string;

  @Column('longtext')
  city: string;

  @Column('text')
  country: string;

  @Column('varchar', { length: 20 })
  zip: string;

  @Column('varchar', { length: 191 })
  image: string;

  @Column('boolean')
  isActive: boolean;

  @Column('varchar', { length: 191 })
  roles: Role[];

  @OneToMany(() => Wallet, wallet => wallet.user)
  wallets: Wallet[];

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @DeleteDateColumn()
  deletedAt: string;

  @VersionColumn()
  version: string;
}
