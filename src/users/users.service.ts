import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import {
  paginate, Pagination, IPaginationOptions,
} from '../pagination';
import { User } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const hashedPassw = await this.hashPassword(createUserDto.password);

    const newUser = this.usersRepository.create({
      email: createUserDto.email,
      password: hashedPassw,
      roles: createUserDto.roles,
      name: createUserDto.name,
      address: createUserDto.address,
      city: createUserDto.city,
      country: createUserDto.country,
      zip: createUserDto.zip,
      image: createUserDto.image,
      isActive: false,
    });
    return this.usersRepository.save(newUser);
  }

  async findAll(paginationOpt: IPaginationOptions): Promise<Pagination<User>> {
    const qb = this.usersRepository.createQueryBuilder('users');
    qb.orderBy('users.id', 'DESC');
    return paginate<User>(qb, paginationOpt);
  }

  findOne(id: number) {
    return this.usersRepository.findOne(id);
  }

  async update(id: number, updateUserDto: UpdateUserDto) {

    if (updateUserDto.password && !updateUserDto.passwordConfirm) {
      throw new BadRequestException;
    }

    if (updateUserDto.password) {
      updateUserDto.password = await this.hashPassword(updateUserDto.password);
    }

    if (updateUserDto.passwordConfirm) {
      delete updateUserDto.passwordConfirm;
    }

    const user = this.usersRepository.update(id, updateUserDto);
    return user;
  }

  async remove(id: number) {
    return this.usersRepository.delete(id);
  }

  async login(username: string) {
    const user = await this.usersRepository
      .createQueryBuilder('user')
      .addSelect('user.password')
      .where('user.email = :email', {email: username})
      .getOne();
    return user;
  }

  async hashPassword(password: string, saltRounds: number = 10) {
    return bcrypt.hash(password, saltRounds);
  }
}
