import {
  IsEmail, IsString, MinLength, IsNotEmpty, IsOptional
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Role } from '../../roles/role.enum';
import { Match } from '../../match.decorator';

export class CreateUserDto {

  @ApiProperty({
    example: 'an.email@somewebsite.com',
    description: 'a valid email'
  })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    example: 'a password',
    description: 'a valid password. 6 characters minimum'
  })
  @IsString()
  @MinLength(6)
  @IsNotEmpty()
  password: string;

  @ApiProperty({
    example: 'a password',
    description: 'must match password.'
  })
  @IsString()
  @MinLength(6)
  @IsNotEmpty()
  @Match('password')
  passwordConfirm: string;

  @ApiProperty({
    example: 'staff',
    description: 'role of the user.'
  })
  @IsString()
  @IsNotEmpty()
  roles: Role[];

  @ApiProperty({
    example: 'John smith',
    description: 'name of the user'
  })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    example: 'john miyer st. no 45',
    description: 'user address'
  })
  @IsString()
  @IsOptional()
  address: string;

  @ApiProperty({
    example: 'california',
    description: 'user city'
  })
  @IsString()
  @IsOptional()
  city: string;

  @ApiProperty({
    example: 'United States of America',
    description: 'user country'
  })
  @IsString()
  @IsOptional()
  country: string;

  @ApiProperty({
    example: '67771',
    description: 'user zip code'
  })
  @IsString()
  @IsOptional()
  zip: string;

  @ApiProperty({
    example: '/img.jpg',
    description: 'user image url'
  })
  @IsString()
  @IsOptional()
  image: string;
}
