import {
  Controller, Get, Post, Body, Put, Param, Delete, Request, UseGuards,
  ForbiddenException, Query, ParseIntPipe
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { Role } from '../roles/role.enum';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UsersService } from './users.service';
import { WalletsService } from '../wallets/wallets.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@ApiBearerAuth()
@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly walletsService: WalletsService
  ) {}

  @Get('me')
  @ApiOperation({ summary: 'Me endpoint'})
  @ApiResponse({
    status: 200,
    description: 'User information bassed on token',
  })
  async me(@Request() req) {
    let response = {
      wallets: 0
    };
    const { user } = req;
    const dbUser = await this.usersService.findOne(+user.id);
    const walletsTotalAmount = await this.walletsService.getTotalAmount(+dbUser.id);

    response.wallets = walletsTotalAmount;
    return response;
  }

  @Post()
  @ApiOperation({ summary: 'Create a user'})
  @ApiResponse({
    status: 200,
    description: 'user is created',
    type: User
  })
  @Roles(Role.Admin)
  async create(@Body() createUserDto: CreateUserDto) {
    const { password, ...newUser } = await this.usersService.create(createUserDto);
    return newUser;
  }

  @Get()
  @ApiOperation({ summary: 'Get users'})
  @ApiResponse({
    status: 200,
    description: 'get users',
  })
  @Roles(Role.Admin)
  async findAll(
    @Query('page', ParseIntPipe) page: number = 1,
    @Query('limit', ParseIntPipe) limit: number = 10,
    @Request() req
  ) {
    limit = limit > 100 ? 100 : limit;
    return this.usersService.findAll({
      page,
      limit,
      route: '/users',
    });
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get a user'})
  @ApiResponse({
    status: 200,
    description: 'get a user',
    type: User
  })
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update a user'})
  @ApiResponse({
    status: 200,
    description: 'user is updated',
    type: User
  })
  @Roles(Role.Admin, Role.Staff)
  async update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @Request() req
  ) {
    const { user } = req;
    if ((user.roles === Role.Staff) && (user.id != id)) {
      throw new ForbiddenException;
    }

    return await this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a user'})
  @ApiResponse({
    status: 200,
    description: 'user is deleted',
  })
  @Roles(Role.Admin, Role.Staff)
  remove(@Param('id') id: string, @Request() req) {
    const { user } = req;
    if ((user.roles === Role.Staff) && (user.id != id)) {
      throw new ForbiddenException;
    }

    return this.usersService.remove(+id);
  }
}
