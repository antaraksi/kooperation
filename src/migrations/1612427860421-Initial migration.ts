import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1612427860421 implements MigrationInterface {
    name = 'InitialMigration1612427860421'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `wallet` (`id` int NOT NULL AUTO_INCREMENT, `amount` bigint NOT NULL, `description` text NOT NULL, `type` varchar(191) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), `deletedAt` datetime(6) NULL, `version` int NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` CHANGE `updatedAt` `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `wallet` ADD CONSTRAINT `FK_35472b1fe48b6330cd349709564` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `wallet` DROP FOREIGN KEY `FK_35472b1fe48b6330cd349709564`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `updatedAt` `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP");
        await queryRunner.query("DROP TABLE `wallet`");
    }

}
